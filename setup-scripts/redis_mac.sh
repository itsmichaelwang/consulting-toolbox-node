# install redis
brew install redis

# configure redis to start on boot
ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents

# start redis using config file
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
