CREATE TABLE job_status (
    job_id SERIAL PRIMARY KEY,
    last_finished_zip VARCHAR(5) NOT NULL DEFAULT '00000',
    search_term TEXT NOT NULL
)