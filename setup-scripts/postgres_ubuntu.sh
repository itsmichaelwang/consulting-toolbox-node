sudo apt-get install postgresql postgresql-contrib postgresql-common
sudo /etc/init.d/postgresql start

# create local user
sudo -u postgres createuser root

# create database for market sizing tool
sudo -u postgres createdb market_sizer

# create table to track scraping jobs
psql -f "$(pwd)/2018-02-19-create-job-status.sql" -d market_sizer -U root

# create table to aggregate search results
psql -f "$(pwd)/2018-02-19-create-search-results.sql" -d market_sizer -U root

# create table to aggregate search results
psql -f "$(pwd)/2018-05-01-alter-job-status-current-status.sql" -d market_sizer -U root

# add column to track last state
psql -f "$(pwd)/2018-06-28-create-current-state-column.sql" -d market_sizer -U root
