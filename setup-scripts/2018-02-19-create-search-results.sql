CREATE TABLE search_results (
    job_id INT,
    listing_id INT,
    repository_type SMALLINT,
    business_name TEXT,
    street TEXT,
    city TEXT,
    state VARCHAR(2),
    zip VARCHAR(5),
    phone TEXT,
    primary_category TEXT,
    website_url TEXT,
    categories TEXT,
    average_rating DECIMAL,
    rating_count INT,
    PRIMARY KEY (job_id, listing_id, repository_type)
)