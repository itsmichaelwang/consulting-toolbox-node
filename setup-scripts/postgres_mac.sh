# setup PostgreSQL
brew install postgresql
pg_ctl -D /usr/local/var/postgres start && brew services start postgresql
postgres -V

# create local user
createuser local --createdb

# create database for market sizing tool
createdb -U local market_sizer

# create table to track scraping jobs
psql -f "$(pwd)/2018-02-19-create-job-status.sql" -d market_sizer -U local

# create table to aggregate search results
psql -f "$(pwd)/2018-02-19-create-search-results.sql" -d market_sizer -U local

# create table to aggregate search results
psql -f "$(pwd)/2018-05-01-alter-job-status-current-status.sql" -d market_sizer -U local

# create table to aggregate search results
psql -f "$(pwd)/2018-05-20-alter-job-status-add-queue-id.sql" -d market_sizer -U local

# add column to track last state
psql -f "$(pwd)/2018-06-28-create-current-state-column.sql" -d market_sizer -U local
