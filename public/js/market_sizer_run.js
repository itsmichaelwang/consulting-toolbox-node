$.when().then(function() {

  var splitUrl = window.location.href.split('/');
  var jobId = splitUrl[splitUrl.length-1];

  $.get('/api/jobStatus/' + jobId, function(jobStatus) {
    const lastFinishedZip = jobStatus.last_finished_zip;
    const lastFinishedState = jobStatus.last_finished_state;
    $('#zipcode').html(`${lastFinishedZip}`);
    $('#state').html(`${lastFinishedState}`);
  });
});
