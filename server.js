var express = require('express');
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();

// app.use(morgan('dev'));             // shows debug messages in node console
app.set('view engine', 'ejs');
app.use('/public', express.static(path.resolve(__dirname, 'public')));  // use public folder for assets
app.use(bodyParser.urlencoded({extended: true}));

require('./app/routes.js')(app);

var port = process.env.PORT || 80;
app.listen(port);
console.log("Starting server @ " + port);
