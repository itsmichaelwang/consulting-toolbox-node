'use strict';

const rp = require('request-promise');

const searchResultsRepository = require('../db/searchResultsRepository');
const jobStatusRepository = require('../db/jobStatusRepository');

const YELLOW_PAGES_REPOSITORY_TYPE = 0;
const MAX_NUMBER_OF_LISTINGS = 50;

module.exports = {
  generateReportForLocation: generateReportForLocation
};

async function generateReportForLocation(searchTerm, jobId, location, maxPage) { // jshint ignore:line
  try {
    let results = [];
    let businesses = await assembleBusinessesInLocation(searchTerm, location, 1, results, maxPage); // jshint ignore:line
    if (businesses.length !== 0) {
      // console.log(`${zipCode} has ${businesses.length} results`);
      for (let i in businesses) {
        await searchResultsRepository.saveBusinesses(jobId, YELLOW_PAGES_REPOSITORY_TYPE, businesses[i]); // jshint ignore:line
      }
    }
    // await jobStatusRepository.updateLastFinishedZip(jobId, location); // jshint ignore:line

  } catch(err) {
    console.log("Error caught in yellowPagesReportService.js");    
    console.log(err);
  }
}

async function assembleBusinessesInLocation(searchTerm, location, pageNum, results, maxPage) { // jshint ignore:line
  try {
    const headers = { 'User-Agent': "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36" };
    const apiUrl = generateApiUrl(searchTerm, location, pageNum);
    console.log(apiUrl);
    
    let apiResponse;
    try {
      apiResponse = await rp({headers: headers, uri: apiUrl, json: true}); // jshint ignore:line
    } catch(e) {
      // console.log("Non-zero status code from YP; retrying...");
      return assembleBusinessesInLocation(searchTerm, location, pageNum, results, maxPage);
    }

    if (apiResponse.searchResult.metaProperties.listingCount !== 0) {
      results = results.concat(apiResponse.searchResult.searchListings.searchListing);
    }
    if (maxPage != undefined && pageNum >= maxPage) {
      return results;
    }
    if (isLastPage(apiResponse)) {
      return results;
    } else {
      return assembleBusinessesInLocation(searchTerm, location, pageNum + 1, results, maxPage);
    }
  } catch(err) {
    console.log("Error caught in yellowPagesReportService.js");
    console.log(err);
  }
}

function generateApiUrl(searchTerm, location, pageNum) {
  const baseUrl = 'http://api2.yp.com/listings/v1/search';
  const params = `?searchloc=${location}&term=${searchTerm}&format=json&sort=name&listingcount=${MAX_NUMBER_OF_LISTINGS}&key=zj8wfdh9qz&pagenum=${pageNum}`;
  return (baseUrl + params).replace(' ', '%20');
}

function isLastPage(res) {
  const number_of_listings = res.searchResult.metaProperties.listingCount;
  return number_of_listings < MAX_NUMBER_OF_LISTINGS;
}
