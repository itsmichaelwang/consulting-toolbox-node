'use strict';

const Excel = require('exceljs');
const jobStatusRepository = require('../db/jobStatusRepository');
const searchResultsRepository = require('../db/searchResultsRepository');

module.exports = {
    generateReport: generateReport
};

async function generateReport(res, jobIds) { // jshint ignore:line
    try {
        var workbook = new Excel.Workbook();
        for (let idx in jobIds) {
            const jobId = jobIds[idx];
            const jobStatus = await jobStatusRepository.getJobStatusById(jobId); // jshint ignore:line
            const searchResults = await searchResultsRepository.getSearchResultsById(jobId); // jshint ignore:line

            const sheetName = jobStatus.rows[0].search_term.substring(0, 31);
            const sheet = workbook.addWorksheet(sheetName);

            sheet.views = [
                {state: 'frozen', xSplit: 0, ySplit: 1}
            ];
            sheet.getRow(1).font = { bold: true };

            sheet.columns = [
                { header: 'Listing ID', key: 'listing_id', width: 11 },
                { header: 'Business Name', key: 'business_name', width: 50 },
                { header: 'Street', key: 'street', width: 42 },
                { header: 'City', key: 'city', width: 17 },
                { header: 'State', key: 'state', width: 6 },
                { header: 'Zip', key: 'zip', width: 7 },
                { header: 'Categories', key: 'categories', width: 84 },
                { header: 'Phone', key: 'phone', width: 13 }
            ];

            for (let idx in searchResults.rows) {
                let searchResult = searchResults.rows[idx];
                if (searchResult == null) {
                    console.log("huh?");
                }
                sheet.addRow([
                    searchResult.listing_id,
                    searchResult.business_name,
                    searchResult.street,
                    searchResult.city,
                    searchResult.state,
                    searchResult.zip,
                    searchResult.categories,
                    searchResult.phone
                ]);
            }
        }

        workbook.xlsx.write(res).then(function() {
            res.end();
        });
    } catch(err) {
        console.log(err);
    }
}