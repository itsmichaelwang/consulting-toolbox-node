'use strict';

let Queue = require('bull');

const yellowPagesReportService = require('../service/yellowPagesReportService');
const jobStatusRepository = require('../db/jobStatusRepository');

const redisConfig = {
  redis: {
    port: 6379,
    host: '127.0.0.1'
  }
};

var yellowPagesScraperQueue = new Queue('yellow pages scraper', redisConfig);

yellowPagesScraperQueue.process(function(job, done) {
  const searchTerm = job.data.searchTerm;
  const jobId = job.data.jobId;
  const zipCode = job.data.zipCode;
  const state = job.data.state;

  if (!searchTerm || !jobId) {
    throw new Error("required fields are empty");
  }
  let operatingMode;  
  let location;
  if (zipCode != undefined) {
    operatingMode = "zipCode";
    location = zipCode;
  } else if (state != undefined) {
    operatingMode = "state";
    location = state;
  } else {
    throw new Error("required operating mode is empty");
  }

  if (operatingMode == "zipCode") {
    console.log(`processing ${jobId} ${searchTerm} ${location}`);
    const generateReportForLocationPromise = yellowPagesReportService.generateReportForLocation(searchTerm, jobId, location);

    generateReportForLocationPromise.then(function() {
      const updateLastFinishedZipPromise = jobStatusRepository.updateLastFinishedZip(jobId, location);
      updateLastFinishedZipPromise.then(function() {
        done();
      });
    });
  } else if (operatingMode == "state") {
    console.log(`processing ${jobId} ${searchTerm} ${location}`);
    const generateReportForLocationPromise = yellowPagesReportService.generateReportForLocation(searchTerm, jobId, location, 3);

    generateReportForLocationPromise.then(function() {
      const updateLastFinishedStatePromise = jobStatusRepository.updateLastFinishedState(jobId, location);
      updateLastFinishedStatePromise.then(function() {
        done();
      });
    });
  }
});

module.exports = {
  yellowPagesScraperQueue: yellowPagesScraperQueue
};
