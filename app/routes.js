'use strict';

const yellowPagesReportService = require('./service/yellowPagesReportService');
const jobStatusRepository = require('./db/jobStatusRepository');
const yellowPagesScraperQueue = require('./jobs/yellowPagesScraperQueue');
const states = require('./util/states');
const reportGenerator = require('./excel/reportGenerator');

module.exports = function(app, passport) {
  /**
   * Landing Page Endpoints
   */
  app.get('/', function(req, res) {
    res.render('repair/index.ejs');
  });

  /**
   * UI Endpoints
   */
  app.get('/market_sizer', function(req, res) {
    res.render('market_sizer.ejs');
  });

  app.get('/market_sizer/run/:jobId', function(req, res) {
    res.render('market_sizer_run.ejs');
  });

  app.get('/market_sizer/dashboard', function(req, res) {
    res.render('market_sizer_dashboard.ejs');
  });

  /**
   * API Endpoints
   */

  // Get information about a job
  app.get('/api/jobStatus/:jobId', function(req, res) {
    const jobId = req.params.jobId;
    const promise = jobStatusRepository.getJobStatusById(jobId);
    promise.then(function(result) {
      res.json(result.rows[0]);
    });
  });

  function leftPad(zipInt) {
    return ("0000" + zipInt).slice(-5);
  }
  
  // Create a new job
  app.post('/api/jobStatus', function(req, res) {
    const searchTerm = req.body.searchTerm;
    const promise = jobStatusRepository.createNewJobStatus(searchTerm);
    promise.then(function(result) {
      const jobId = result.rows[0].job_id;
      const searchTerm = result.rows[0].search_term;

      for (let i = 0; i < states.length; i++) {
        yellowPagesScraperQueue.yellowPagesScraperQueue.add({
          searchTerm: searchTerm,
          jobId: jobId,
          state: states[i].name
        });
      }
      
      res.redirect(`/market_sizer/run/${jobId}`);
    });
  });

  // Update a job
  app.post('/api/jobStatus/:jobId', function(req, res) {
    const jobId = req.params.jobId;
    const lastFinishedZip = req.body.lastFinishedZip;
    const currentStatus = req.body.currentStatus;

    if (!jobId) {
      throw new Error("required field is empty");
    }

    let promises = [];
    if (lastFinishedZip) {
      promises.push(jobStatusRepository.updateLastFinishedZip(jobId, lastFinishedZip));
    }

    if (currentStatus) {
      switch (currentStatus) {
        case "0": {
          promises.push(jobStatusRepository.pauseJob(jobId));
          break;
        }
        case "1": {
          promises.push(jobStatusRepository.startJob(jobId));
          break;
        }
        case "2": {
          promises.push(jobStatusRepository.finishJob(jobId));
          break;
        }
      }
    }

    Promise.all(promises).then(function(result) {
      res.json(result);
    });    
  });

  // Fetch search results for a job
  app.post('/api/searchResults/', function(req, res) {
    const searchTerm = req.body.searchTerm;
    const jobId = req.body.jobId;
    const zipCode = req.body.zipCode;
    if (!searchTerm || !jobId || !zipCode) {
      throw new Error("required field is empty");
    }

    const promise = yellowPagesReportService.generateReportForLocation(searchTerm, jobId, zipCode);
    promise.then(function(result) {
      res.status(200).json({
        searchTerm: searchTerm,
        jobId: jobId,
        zipCode: zipCode
      });
    });
  });

  app.get('/api/reports/:jobIds', function(req, res) {
    const jobIds = req.params.jobIds.split(",");
    if (!jobIds || jobIds.length == 0) {
      throw new Error("required field is empty");
    }

    var fileName = 'report.xlsx';
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", "attachment; filename=" + fileName);

    const reportGeneratorPromise = reportGenerator.generateReport(res, jobIds);
  });
};
