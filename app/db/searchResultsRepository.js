'use strict';

const dbPool = require('./dbPool');

module.exports = {
  saveBusinesses: saveBusinesses,
  getSearchResultsById: getSearchResultsById
};

function saveBusinesses(job_id, repository_type, business) {  // jshint ignore:line
  const template =
    "INSERT INTO public.search_results " +
    "(job_id, listing_id, repository_type, business_name, street, city, state, zip, phone, primary_category, website_url, categories, average_rating, rating_count) " +
    "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) " +
    "ON CONFLICT DO NOTHING";
  return dbPool.query(template, [job_id, business.listingId, repository_type, business.businessName, business.street, business.city, business.state, business.zip, business.phone, business.primaryCategory, business.websiteURL, business.categories.replace(/\|/g, ","), business.averageRating, business.ratingCount]);
}

function getSearchResultsById(job_id) {
  return dbPool.query("SELECT * FROM public.search_results WHERE job_id = ($1)", [job_id]);
}
