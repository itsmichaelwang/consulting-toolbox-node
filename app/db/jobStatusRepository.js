'use strict';

const dbPool = require('./dbPool');

module.exports = {
  createNewJobStatus: createNewJobStatus,
  getJobStatusById: getJobStatusById,
  updateLastFinishedZip: updateLastFinishedZip,
  updateLastFinishedState: updateLastFinishedState,
  pauseJob: pauseJob,
  startJob: startJob,
  finishJob: finishJob
};

function createNewJobStatus(search_term) {
  return dbPool.query("INSERT INTO public.job_status (search_term) VALUES ($1) RETURNING *", [search_term]);
}

function getJobStatusById(jobId) {
  return dbPool.query("SELECT * FROM public.job_status WHERE job_id = ($1)", [jobId]);
}

function updateLastFinishedZip(jobId, lastFinishedZip) {
  return dbPool.query("UPDATE public.job_status SET last_finished_zip = ($1) WHERE job_id = ($2) RETURNING *", [lastFinishedZip, jobId]);
}

function updateLastFinishedState(jobId, lastFinishedState) {
  return dbPool.query("UPDATE public.job_status SET last_finished_state = ($1) WHERE job_id = ($2) RETURNING *", [lastFinishedState, jobId]);
}

function pauseJob(jobId) {
  return dbPool.query("UPDATE public.job_status SET current_status = 0 WHERE job_id = ($1) RETURNING *", [jobId]);
}

function startJob(jobId) {
  return dbPool.query("UPDATE public.job_status SET current_status = 1 WHERE job_id = ($1) RETURNING *", [jobId]);
}

function finishJob(jobId) {
  return dbPool.query("UPDATE public.job_status SET current_status = 2 WHERE job_id = ($1) RETURNING *", [jobId]);
}
