'use strict';

const { Pool } = require('pg');

let pool;
if (process.env.SERVER_ENV == "dev") {
  pool = new Pool({
    user: 'local',
    host: 'localhost',
    database: 'market_sizer',
    port: 5432,
  }); 
} else if (process.env.SERVER_ENV == "prod") {
  pool = new Pool({
    user: 'root',
    password: process.env.PROD_DB_PASSWORD,
    host: 'localhost',
    database: 'market_sizer',
    port: 5432,
  });
} else {
  throw new Error("environment not specified; unable to determine correct database settings");
}

module.exports = pool;
